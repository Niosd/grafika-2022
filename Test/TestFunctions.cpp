#include <cmath>
#include "TestFunctions.h"
#include "../Core/Constants.h"

using namespace cagd;
using namespace std;

GLdouble spiral_on_cone::u_min = -TWO_PI;
GLdouble spiral_on_cone::u_max = +TWO_PI;

DCoordinate3 spiral_on_cone::d0(GLdouble u)
{
    return DCoordinate3(u * cos(u), u * sin(u), u);
}

DCoordinate3 spiral_on_cone::d1(GLdouble u)
{
    GLdouble c = cos(u), s = sin(u);
    return DCoordinate3(c - u * s, s + u * c, 1.0);
}

DCoordinate3 spiral_on_cone::d2(GLdouble u)
{
    GLdouble c = cos(u), s = sin(u);
    return DCoordinate3(-2.0 * s - u * c, 2.0 * c - u * s, 0);
}

//torus

GLdouble torus::u_min = 0.0;
GLdouble torus::u_max = 6*PI;

DCoordinate3 torus::d0(GLdouble u)
{
    return DCoordinate3((2+cos(2*u/3))*cos(u), (2+cos(2*u/3))*sin(u), sin(2*u/3));
}

DCoordinate3 torus::d1(GLdouble u)
{
    return DCoordinate3(-(2+cos(2*u/3))*sin(u)-2.0/3*sin(2*u/3)*cos(u), (2+cos(2*u/3))*cos(u)-2.0/3*sin(2*u/3)*sin(u), 2.0/3*cos(2*u/3));
}

DCoordinate3 torus::d2(GLdouble u)
{
    return DCoordinate3(1.33*sin((2*u)/3)*sin(u)-0.44*cos((2*u)/3)*cos(u)-(cos((2*u)/3)+2)*cos(u),
                        -1.33*sin((2*u)/3)*cos(u)-0.44*cos((2*u)/3)*sin(u)-(cos((2*u)/3)+2)*sin(u),
                        -0.44*sin(2*u)/3);
}

//ellipse

GLdouble ellipse::u_min = -PI;
GLdouble ellipse::u_max = +PI;

DCoordinate3 ellipse::d0(GLdouble u)
{
    return DCoordinate3(8 * cos(u), 4 * sin(u), 0);
}
DCoordinate3 ellipse::d1(GLdouble u)
{
    return DCoordinate3(-8 * sin(u), 4 * cos(u), 0);
}
DCoordinate3 ellipse::d2(GLdouble u)
{
    return DCoordinate3(-8 * cos(u), -4 * sin(u), 0);
}

GLdouble epicycloid::u_min = 0;
GLdouble epicycloid::u_max = TWO_PI;
GLdouble epicycloid::a = 4;
GLdouble epicycloid::b = 1;

DCoordinate3 epicycloid::d0(GLdouble u)
{
    GLdouble c = cos((a + b) / b * u), s =  sin((a + b) / b * u);
    return DCoordinate3((b + a) * cos(u) - b * c, (b + a) * sin(u) - b * s, 0);
}

DCoordinate3 epicycloid::d1(GLdouble u)
{
    GLdouble c = cos((a + b) / b * u), s =  sin((a + b) / b * u);
    return DCoordinate3((b + a) * (s - sin(u)),-(b + a) * (c - cos(u)), 0);
}

DCoordinate3 epicycloid::d2(GLdouble u)
{
    GLdouble c = cos((a + b) / b * u), s =  sin((a + b) / b * u);
    return DCoordinate3((b + a) * ((b + a) / b * c - cos(u)), -(b + a) * (sin(u) - (b + a) / b * s), 0);
}

GLdouble lissajous::u_min = 0;
GLdouble lissajous::u_max = TWO_PI;
GLdouble lissajous::a = 6;
GLdouble lissajous::b = 5;
GLdouble lissajous::n = 6;

DCoordinate3 lissajous::d0(GLdouble u)
{
    GLdouble c = cos(u), s =  sin(u);
    return DCoordinate3(a * c, a * s, b * sin(n * u));
}

DCoordinate3 lissajous::d1(GLdouble u)
{
    GLdouble c = cos(u), s =  sin( u);
    return DCoordinate3(-a * s,a * c, b * n * cos(n * u));
}

DCoordinate3 lissajous::d2(GLdouble u)
{
    GLdouble c = cos(u), s =  sin(u);
    return DCoordinate3(-a * c, -a * s, -b * n * n * sin(n * u));
}

GLdouble rose::u_min = 0;
GLdouble rose::u_max = TWO_PI;
GLdouble rose::a = 10;
GLdouble rose::b = 7;

DCoordinate3 rose::d0(GLdouble u)
{
    GLdouble c = cos(u), s =  sin(u), cb = cos(b * u);
    return DCoordinate3(a * cb * c, a * cb * s, 0);
}

DCoordinate3 rose::d1(GLdouble u)
{
    GLdouble c = cos(u), s =  sin( u);
    return DCoordinate3(-a * (b * c * sin(b * u) + s * cos(b * u)),-a * (b * s * sin(b * u) - c * cos(b * u)), 0);
}

DCoordinate3 rose::d2(GLdouble u)
{
    GLdouble c = cos(u), s =  sin(u);
    return DCoordinate3(a * (2 * b * s * sin(b * u) + (-b * b -1) * c * cos(b * u)),-a * (2 * b * c * sin(b * u) + (b * b + 1)* s * cos(b * u)),0);
}

//hypo

GLdouble hypo::u_min = -3.0;
GLdouble hypo::u_max = +3.0;

DCoordinate3 hypo::d0(GLdouble u)
{
    return DCoordinate3(5*cos(u)+cos(5*u), 5*sin(u)-sin(5*u), 0);
}

DCoordinate3 hypo::d1(GLdouble u)
{
    return DCoordinate3(-5*sin(u)-5*sin(5*u), 5*cos(u)-5*cos(5*u), 0);
}

DCoordinate3 hypo::d2(GLdouble u)
{
    return DCoordinate3(-5*cos(u)-25*cos(5*u), -5*sin(u)+25*sin(5*u), 0);
}

//helix

GLdouble helix::u_min = 0;
GLdouble helix::u_max = 24*PI;

DCoordinate3 helix::d0(GLdouble u)
{
    return 0.2* DCoordinate3(cos(u),sin(u), u/7);
}

DCoordinate3 helix::d1(GLdouble u)
{
    return 0.2* DCoordinate3(-sin(u),cos(u), 1/7);
}

DCoordinate3 helix::d2(GLdouble u)
{
    return 0.2* DCoordinate3(-cos(u),-sin(u), 0);
}



