#pragma once

#include <GL/glew.h>
#include <QOpenGLWidget>
#include "Core/GenericCurves3.h"
#include "Parametric/ParametricCurves3.h"
#include "Core/Exceptions.h"
#include <memory>


namespace cagd
{
    class GLWidget: public QOpenGLWidget
    {
        Q_OBJECT

    private:

        // variables defining the projection matrix
        double       _aspect;            // aspect ratio of the rendering window
        double       _fovy;              // field of view in direction y
        double       _z_near, _z_far;    // distance of near and far clipping planes

        // variables defining the model-view matrix
        double       _eye[3], _center[3], _up[3];

        // variables needed by transformations
        int         _angle_x, _angle_y, _angle_z;
        double      _zoom;
        double      _trans_x, _trans_y, _trans_z;

        // your other declarations

        // gui
        GLuint  _img_select;
        GLuint  _img_index;

        bool draw_d1;
        bool draw_d2;

        GLenum _usage_flag;
        GLuint _div_point_count;
        GLuint _v_div_point_count;

        //curves
        ParametricCurve3*   _pc[8];
        GenericCurve3*      _image_of_pc[8];

    public:
        // special and default constructor
        // the format specifies the properties of the rendering window
        GLWidget(QWidget* parent = 0);

        // redeclared virtual functions
        void initializeGL();
        void paintGL();
        void resizeGL(int w, int h);

    public slots:
        // public event handling methods/slots
        void set_angle_x(int value);
        void set_angle_y(int value);
        void set_angle_z(int value);

        void set_zoom_factor(double value);

        void set_trans_x(double value);
        void set_trans_y(double value);
        void set_trans_z(double value);

        void set_img(int value);
        void set_index(int value);

        void set_draw_d1(bool value);
        void set_draw_d2(bool value);

    };
}
