#include "GLWidget.h"

#if !defined(__APPLE__)
#include <GL/glu.h>
#endif

#include <iostream>
using namespace std;

#include <Core/Exceptions.h>
#include "Core/Constants.h"
#include "Core/Matrices.h"
#include "Test/TestFunctions.h"

namespace cagd
{
    //--------------------------------
    // special and default constructor
    //--------------------------------
    GLWidget::GLWidget(QWidget *parent): QOpenGLWidget(parent)
    {
    }

    //--------------------------------------------------------------------------------------
    // this virtual function is called once before the first call to paintGL() or resizeGL()
    //--------------------------------------------------------------------------------------
    void GLWidget::initializeGL()
    {
        // creating a perspective projection matrix
        glMatrixMode(GL_PROJECTION);

        glLoadIdentity();

        _aspect = static_cast<double>(width()) / static_cast<double>(height());
        _z_near = 1.0;
        _z_far  = 1000.0;
        _fovy   = 45.0;

        gluPerspective(_fovy, _aspect, _z_near, _z_far);

        // setting the model view matrix
        glMatrixMode(GL_MODELVIEW);
        glLoadIdentity();

        _eye[0] = _eye[1] = 0.0; _eye[2] = 6.0;
        _center[0] = _center[1] = _center[2] = 0.0;
        _up[0] = _up[2] = 0.0; _up[1] = 1.0;

        gluLookAt(_eye[0], _eye[1], _eye[2], _center[0], _center[1], _center[2], _up[0], _up[1], _up[2]);

        // enabling the depth test
        glEnable(GL_DEPTH_TEST);

        // setting the background color
        glClearColor(0.0f, 0.0f, 0.0f, 1.0f);

        // initial values of transformation parameters
        _angle_x = _angle_y = _angle_z = 0.0;
        _trans_x = _trans_y = _trans_z = 0.0;
        _zoom = 1.0;

        // ...

        try
        {
            // initializing the OpenGL Extension Wrangler library
            GLenum error = glewInit();

            if (error != GLEW_OK)
            {
                throw Exception("Could not initialize the OpenGL Extension Wrangler Library!");
            }

            if (!glewIsSupported("GL_VERSION_2_0"))
            {
                throw Exception("Your graphics card is not compatible with OpenGL 2.0+! "
                                "Try to update your driver or buy a new graphics adapter!");
            }

            // create and store your geometry in display lists or vertex buffer objects
            // ...

            _div_point_count = 100;
            _v_div_point_count = 200;
            _usage_flag = GL_STATIC_DRAW;

            // PARAMETRIC CURVE

            {
                {
                    //spiral on cone
                    RowMatrix<ParametricCurve3::Derivative> derivative(3);
                    derivative(0) = spiral_on_cone::d0;
                    derivative(1) = spiral_on_cone::d1;
                    derivative(2) = spiral_on_cone::d2;

                    _pc[0] = new ParametricCurve3(derivative, spiral_on_cone::u_min, spiral_on_cone::u_max);
                    if(!_pc[0])
                    {
                        throw Exception("Parametric curve letrehozas hiba");
                    }

                    _image_of_pc[0] = _pc[0]->GenerateImage(_div_point_count, _usage_flag);

                    if(!_image_of_pc[0])
                    {
                        throw Exception("Parametric curve image letrehozas hiba");
                    }

                    if(!_image_of_pc[0]->UpdateVertexBufferObjects(_usage_flag))
                    {
                        throw Exception("Could not create the vertex buffer object of the parametric curve!");
                    }
                }
                {
                    //torus
                    RowMatrix<ParametricCurve3::Derivative> derivative(3);
                    derivative(0) = torus::d0;
                    derivative(1) = torus::d1;
                    derivative(2) = torus::d2;

                    _pc[1] = new ParametricCurve3(derivative, torus::u_min, torus::u_max);
                    if(!_pc[1])
                    {
                        throw Exception("Parametric curve letrehozas hiba");
                    }

                    _image_of_pc[1] = _pc[1]->GenerateImage(_div_point_count, _usage_flag);

                    if(!_image_of_pc[1])
                    {
                        throw Exception("Parametric curve image letrehozas hiba");
                    }

                    if(!_image_of_pc[1]->UpdateVertexBufferObjects(_usage_flag))
                    {
                        throw Exception("Could not create the vertex buffer object of the parametric curve!");
                    }

                }
                {
                    //ellipse
                    RowMatrix<ParametricCurve3::Derivative> derivative(3);
                    derivative(0) = ellipse::d0;
                    derivative(1) = ellipse::d1;
                    derivative(2) = ellipse::d2;

                    _pc[2] = new ParametricCurve3(derivative, ellipse::u_min, ellipse::u_max);
                    if(!_pc[2])
                    {
                        throw Exception("Parametric curve letrehozas hiba");
                    }

                    _image_of_pc[2] = _pc[2]->GenerateImage(_div_point_count, _usage_flag);

                    if(!_image_of_pc[2])
                    {
                        throw Exception("Parametric curve image letrehozas hiba");
                    }

                    if(!_image_of_pc[2]->UpdateVertexBufferObjects(_usage_flag))
                    {
                        throw Exception("Could not create the vertex buffer object of the parametric curve!");
                    }
                }
                {
                    //epicycloid
                    RowMatrix<ParametricCurve3::Derivative> derivative(3);
                    derivative(0) = epicycloid::d0;
                    derivative(1) = epicycloid::d1;
                    derivative(2) = epicycloid::d2;

                    _pc[3] = new ParametricCurve3(derivative, ellipse::u_min, ellipse::u_max);
                    if(!_pc[3])
                    {
                        throw Exception("Parametric curve letrehozas hiba");
                    }

                    _image_of_pc[3] = _pc[3]->GenerateImage(_div_point_count, _usage_flag);

                    if(!_image_of_pc[3])
                    {
                        throw Exception("Parametric curve image letrehozas hiba");
                    }

                    if(!_image_of_pc[3]->UpdateVertexBufferObjects(_usage_flag))
                    {
                        throw Exception("Could not create the vertex buffer object of the parametric curve!");
                    }
                }
                {
                    //lissajous
                    RowMatrix<ParametricCurve3::Derivative> derivative(3);
                    derivative(0) = lissajous::d0;
                    derivative(1) = lissajous::d1;
                    derivative(2) = lissajous::d2;

                    _pc[4] = new ParametricCurve3(derivative, ellipse::u_min, ellipse::u_max);
                    if(!_pc[4])
                    {
                        throw Exception("Parametric curve letrehozas hiba");
                    }

                    _image_of_pc[4] = _pc[4]->GenerateImage(_div_point_count, _usage_flag);

                    if(!_image_of_pc[4])
                    {
                        throw Exception("Parametric curve image letrehozas hiba");
                    }

                    if(!_image_of_pc[4]->UpdateVertexBufferObjects(_usage_flag))
                    {
                        throw Exception("Could not create the vertex buffer object of the parametric curve!");
                    }
                }
                {
                    //ellipse
                    RowMatrix<ParametricCurve3::Derivative> derivative(3);
                    derivative(0) = rose::d0;
                    derivative(1) = rose::d1;
                    derivative(2) = rose::d2;

                    _pc[5] = new ParametricCurve3(derivative, ellipse::u_min, ellipse::u_max);
                    if(!_pc[5])
                    {
                        throw Exception("Parametric curve letrehozas hiba");
                    }

                    _image_of_pc[5] = _pc[5]->GenerateImage(_div_point_count, _usage_flag);

                    if(!_image_of_pc[5])
                    {
                        throw Exception("Parametric curve image letrehozas hiba");
                    }

                    if(!_image_of_pc[5]->UpdateVertexBufferObjects(_usage_flag))
                    {
                        throw Exception("Could not create the vertex buffer object of the parametric curve!");
                    }
                }


                {
                    //hypo
                    RowMatrix<ParametricCurve3::Derivative> derivative(3);
                    derivative(0) = hypo::d0;
                    derivative(1) = hypo::d1;
                    derivative(2) = hypo::d2;

                    _pc[6] = new ParametricCurve3(derivative, hypo::u_min, hypo::u_max);
                    if(!_pc[6])
                    {
                        throw Exception("Parametric curve letrehozas hiba");
                    }

                    _image_of_pc[6] = _pc[6]->GenerateImage(_div_point_count, _usage_flag);

                    if(!_image_of_pc[6])
                    {
                        throw Exception("Parametric curve image letrehozas hiba");
                    }

                    if(!_image_of_pc[6]->UpdateVertexBufferObjects(_usage_flag))
                    {
                        throw Exception("Could not create the vertex buffer object of the parametric curve!");
                    }
                }
                {
                    //helix
                    RowMatrix<ParametricCurve3::Derivative> derivative(3);
                    derivative(0) = helix::d0;
                    derivative(1) = helix::d1;
                    derivative(2) = helix::d2;

                    _pc[7] = new ParametricCurve3(derivative, helix::u_min, helix::u_max);
                    if(!_pc[7])
                    {
                        throw Exception("Parametric curve letrehozas hiba");
                    }

                    _image_of_pc[7] = _pc[7]->GenerateImage(_div_point_count, _usage_flag);

                    if(!_image_of_pc[7])
                    {
                        throw Exception("Parametric curve image letrehozas hiba");
                    }

                    if(!_image_of_pc[7]->UpdateVertexBufferObjects(_usage_flag))
                    {
                        throw Exception("Could not create the vertex buffer object of the parametric curve!");
                    }
                }
            }


        }
        catch (Exception &e)
        {
            cout << e << endl;
        }
    }

    //-----------------------
    // the rendering function
    //-----------------------
    void GLWidget::paintGL()
    {
        // clears the color and depth buffers
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        // stores/duplicates the original model view matrix
        glPushMatrix();

            // applying transformations
            glRotatef(_angle_x, 1.0, 0.0, 0.0);
            glRotatef(_angle_y, 0.0, 1.0, 0.0);
            glRotatef(_angle_z, 0.0, 0.0, 1.0);
            glTranslated(_trans_x, _trans_y, _trans_z);
            glScaled(_zoom, _zoom, _zoom);

            // render your geometry (this is oldest OpenGL rendering technique, later we will use some advanced methods)



            switch(_img_select){
                case 0: //Triangle

                    glColor3f(1.0f, 1.0f, 1.0f);
                    glBegin(GL_LINES);
                        glVertex3f(0.0f, 0.0f, 0.0f);
                        glVertex3f(1.1f, 0.0f, 0.0f);

                        glVertex3f(0.0f, 0.0f, 0.0f);
                        glVertex3f(0.0f, 1.1f, 0.0f);

                        glVertex3f(0.0f, 0.0f, 0.0f);
                        glVertex3f(0.0f, 0.0f, 1.1f);
                    glEnd();

                    glBegin(GL_TRIANGLES);
                        // attributes
                        glColor3f(1.0f, 0.0f, 0.0f);
                        // associated with position
                        glVertex3f(1.0f, 0.0f, 0.0f);

                        // attributes
                        glColor3f(0.0, 1.0, 0.0);
                        // associated with position
                        glVertex3f(0.0, 1.0, 0.0);

                        // attributes
                        glColor3f(0.0f, 0.0f, 1.0f);
                        // associated with position
                        glVertex3f(0.0f, 0.0f, 1.0f);
                    glEnd();

                    break;

                case 1: // render parametric curve
                    if(_image_of_pc[_img_index]) {

                        glColor3f(1.0, 0.0, 0.0);
                        glPointSize(5.0);
                        _image_of_pc[_img_index]->RenderDerivatives(0, GL_LINE_STRIP);

                        glPointSize(1.0);

                        if(draw_d1){
                            glColor3f(0.0 , 1.0, 0.0);
                            _image_of_pc[_img_index]->RenderDerivatives(1, GL_LINES);
                            _image_of_pc[_img_index]->RenderDerivatives(1, GL_POINTS);
                        }

                        if(draw_d2){
                            glColor3f(0.0 , 0.0, 1.0);
                            _image_of_pc[_img_index]->RenderDerivatives(2, GL_LINES);
                            _image_of_pc[_img_index]->RenderDerivatives(2, GL_POINTS);
                        }
                    }
                    break;
            }

        // pops the current matrix stack, replacing the current matrix with the one below it on the stack,
        // i.e., the original model view matrix is restored
        glPopMatrix();
    }

    //----------------------------------------------------------------------------
    // when the main window is resized one needs to redefine the projection matrix
    //----------------------------------------------------------------------------
    void GLWidget::resizeGL(int w, int h)
    {
        // setting the new size of the rendering context
        glViewport(0, 0, w, h);

        // redefining the projection matrix
        glMatrixMode(GL_PROJECTION);

        glLoadIdentity();

        _aspect = static_cast<double>(w) / static_cast<double>(h);

        gluPerspective(_fovy, _aspect, _z_near, _z_far);

        // switching back to the model view matrix
        glMatrixMode(GL_MODELVIEW);

        update();
    }

    //-----------------------------------
    // implementation of the public slots
    //-----------------------------------

    void GLWidget::set_angle_x(int value)
    {
        if (_angle_x != value)
        {
            _angle_x = value;
            update();
        }
    }

    void GLWidget::set_angle_y(int value)
    {
        if (_angle_y != value)
        {
            _angle_y = value;
            update();
        }
    }

    void GLWidget::set_angle_z(int value)
    {
        if (_angle_z != value)
        {
            _angle_z = value;
            update();
        }
    }

    void GLWidget::set_zoom_factor(double value)
    {
        if (_zoom != value)
        {
            _zoom = value;
            update();
        }
    }

    void GLWidget::set_trans_x(double value)
    {
        if (_trans_x != value)
        {
            _trans_x = value;
            update();
        }
    }

    void GLWidget::set_trans_y(double value)
    {
        if (_trans_y != value)
        {
            _trans_y = value;
            update();
        }
    }

    void GLWidget::set_trans_z(double value)
    {
        if (_trans_z != value)
        {
            _trans_z = value;
            update();
        }
    }

    void GLWidget::set_img(int value)
    {
        if (_img_select != value)
        {
            _img_select = value;
            update();
        }
    }

    void GLWidget::set_index(int value)
    {
        if (_img_index != value)
        {
            _img_index = value;
            update();
        }
    }

    void GLWidget::set_draw_d1(bool value)
    {
        if (draw_d1 != value)
        {
            draw_d1 = value;
            update();
        }
    }

    void GLWidget::set_draw_d2(bool value)
    {
        if (draw_d2 != value)
        {
            draw_d2 = value;
            update();
        }
    }

}
